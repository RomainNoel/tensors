# Manifest for tensors

This file is a listing of all files considered to be part of this package.
It is automatically generated with `l3build manifest`.


## Repository manifest

The following groups list the files included in the development repository of the package.
Files listed with a ‘†’ marker are included in the TDS but not CTAN files, and files listed
with ‘‡’ are included in both.

### Source files

These are source files for a number of purposes, including the `unpack` process which
generates the installation files of the package. Additional files included here will also
be installed for processing such as testing.

* tensors.dtx ‡
* tensors.ins ‡

### Derived files

The files created by ‘unpacking’ the package sources. This typically includes
`.sty` and `.cls` files created from DocStrip `.dtx` files.

* tensors.sty †

### Test files

These files form the test suite for the package. `.lvt` or `.lte` files are the individual
unit tests, and `.tlg` are the stored output for ensuring changes to the package produce
the same output. These output files are sometimes shared and sometime specific for
different engines (pdfTeX, XeTeX, LuaTeX, etc.).

* tensors-test-001.lvt 
* tensors-test-002.lvt 
* tensors-test-001.lve 
* tensors-test-002.luatex.tlg 
* tensors-test-002.tlg 
* tensors-test-002.xetex.tlg 


## TDS manifest

The following groups list the files included in the TeX Directory Structure used to install
the package into a TeX distribution.

### Source files (TDS)

All files included in the `tensors/source` directory.

* tensors.dtx 
* tensors.ins 

### TeX files (TDS)

All files included in the `tensors/tex` directory.

* tensors.sty 

### Doc files (TDS)

All files included in the `tensors/doc` directory.

* CTANREADME.md 
* tensors-doc.pdf 
* tensors-doc.tex 
* tensors-example.pdf 
* tensors-example.tex 
* tensors.pdf 


## CTAN manifest

The following group lists the files included in the CTAN package.

### CTAN files

* tensors-doc.pdf 
* tensors-doc.tex 
* tensors-example.pdf 
* tensors-example.tex 
* tensors.dtx 
* tensors.ins 
* tensors.pdf 
