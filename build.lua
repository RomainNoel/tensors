#!/usr/bin/env texlua

--[[
  ** Build config for tensors using l3build **
--]]

-- Identification
module     = "tensors"
pkgversion = "0.1.1.2" -- Major, Minor, Patch
pkgdate    = "2025-02-17"

-- 
-- CONFIGURATION of files for build and installation
-- ------------------------
maindir       = "." -- Top level directory for the module/bundle
sourcefiledir = "./src" -- Directory containing source files
sourcefiles   = {"*.dtx", "*.ins"} -- "sources.pdf"-- Files to copy for unpacking
installfiles  = {"*.sty", "*.cls", "*.pdf",} -- Files to install to the tex area of the texmf tree
supportdir = "./support" -- general support files
-- local fake_tds = "build/distrib" -- Define the fake TDS directory


-- 
-- UNPACKING
-- ------------------------

-- Unpacking files from .dtx file
unpackfiles = {""..module..".ins"} -- Files to run to perform unpacking
unpackopts  = "--interaction=batchmode" -- Options passed to engine when unpacking
-- unpackexe   = "luatex" -- Executable for running unpack
-- unpacksuppfiles = {""..module.."-logo.pdf", } -- Files needed to support unpacking when “sandboxed”


-- 
-- DOCUMENTATION + EXAMPLES
-- ------------------------

-- Generating documentation
-- docfiledir = "./doc" -- Directory containing documentation files
docfiles = {"examples/*.tex", "doc/"..module.."-doc.tex"} --Files which are part of the documentation and should be copy into doc.
demofiles = {"examples/*.tex"} -- Files which show how to use a module
-- typesetdemofiles = {"examples/"..module.."-example.tex"} -- Files to typeset before the documentation for inclusion in main documentation files

typesetfiles  = { ""..module.."-example.tex", ""..module..".dtx", ""..module.."-user-cmds.tex", ""..module.."-dev-impl.tex", ""..module.."-doc.tex", } --""..module..".ins", -- Files to typeset for documentation
typesetopts   = "-interaction=batchmode --shell-escape" -- Options passed to engine when typesetting
typesetsuppfiles = {""..module.."-logo.pdf", "*.png"} -- Files that match typesetsuppfiles in the support directory (supportdir) are copied into the build/doc directory
-- typesetexe    = "pdflatex" -- Executable for compiling doc(s)
-- typesetopts   = "-interaction=batchmode --shell-escape" -- Options passed to engine when typesetting
-- typesetruns   = 3 -- Number of cycles of typesetting to carry out
-- typesetcmds = "" -- Instructions to be passed to TEX when doing typesetting


-- 
-- UNIT TESTS
-- ------------------------

testfiledir = "./testfiles" --"./testfiles/1run" -- Directory containing test files
-- testsuppdir = "./testfiles/support" -- Directory containing test-specific support files
checksearch = true -- Enable command printing during check phase
checkruns=1
checkconfigs = {"build"} -- , "./testfiles/config2runs", "./testfiles/config2runs+bib"


-- 
-- PACKAGING FOR CTAN
-- ------------------------

-- textfiledir = maindir -- Directory containing plain text files
ctanInputFolder = maindir .."/ctan" -- Custom folder with files for CTAN 
textfiles = { "MANIFEST.md", "README.md", ctanInputFolder.."/*.*"} -- Plain text files to send to CTAN as-is

-- Update package date and version
tagfiles = {sourcefiledir.."/"..module..".dtx", "README.md", "doc/"..module.."-doc.tex", ctanInputFolder.."/ctan.ann"}

-- Configuration for ctan
-- ctanreadme = ctanInputFolder .."/CTANREADME.md" --
-- manifestfile = "MANIFEST.md" -- 
ctanpkg    = module
-- ctandir = distribdir .. "/ctan" -- Directory for organising the OUTPUT files for CTAN
ctanzip    = ctanpkg.."-"..pkgversion.."-ctan"
-- ctanfiles = {}
-- 
uploadconfig = {
  author      = "Romain NOEL",
  uploader    = "Romain NOEL",
  email       = "romainoel@free.fr",
  pkg         = ctanpkg,
  version     = pkgversion,
  license     = "lppl1.3c",
  summary     = "This package provides a generic customable display of tensors.",
  description = [[ This package let you display your tensors according to their order in a generic way. This let you choose and change options of display according to the context. ]],
  topic       = { "latex3", "expl3", "maths" }, -- keywords from https://ctan.org/topics/highscore
  ctanPath    = "/macros/latex/contrib/beamer-contrib/themes" .. ctanpkg,
  home        = "https://gitlab.com/RomainNOEL/tensors/",
  repository  = "https://gitlab.com/RomainNOEL/tensors/",
  development = "https://gitlab.com/RomainNOEL/tensors/",
  bugtracker  = "https://gitlab.com/RomainNOEL/tensors".."/issues",
  support     = "https://gitlab.com/RomainNOEL/tensors".."/issues",
  announcement_file = ctanInputFolder .."/ctan.ann",
  note_file   = ctanInputFolder .."/ctan.note",
  update      = true, -- does the pkg exists on CTAN
}


-- 
-- EXTRAS FUNCTIONS like TAGGING
-- -----------------------------

-- Clean files
cleanfiles = {
  ctanzip..".curlopt",
  ctanzip..".zip",
  "*.log",
  ""..module.."-example*.pdf",
  ""..module..".pdf",
  ""..module.."-*.pdf",
}

-- Define the files and their specific patterns
files2tag = {
  {
    name = "README.md",
    version_pattern = "Latest_Release%-v%d+%.%d+%.%d+",
    date_pattern = "Date: %d%d%d%d%-%d%d%-%d%d",
    version_output = "Latest_Release-v".. string.match(pkgversion, "(%d+%.%d+%.%d+)"),
    date_output = "Date: " .. pkgdate
  },
  {
    name = "src/tensors.dtx",
    
    version_pattern = "ProvidesExplPackage{"..module.."}{%d%d%d%d%-%d%d%-%d%d}{%d+%.%d+%.%d+%}",
    version_subpattern = "}{%d+%.%d+%.%d+%}",
    date_pattern = "ProvidesExplPackage{"..module.."}{%d%d%d%d%-%d%d%-%d%d}{%d+%.%d+%.%d+%}",
    date_subpattern = module.."}{%d%d%d%d%-%d%d%-%d%d}{",
    
    version_output =  "}{".. pkgversion .."}",
    date_output = module.."}{"..pkgdate.."}{"
  },
  {
    name = "src/tensors.dtx",
    
    version_pattern = module.."Version{v%d+%.%d+%.%d+%}",
    date_pattern = module.."Date{%d%d%d%d%-%d%d%-%d%d}",
    
    version_output =  module.."Version{v".. pkgversion .."}",
    date_output = module.."Date{"..pkgdate.."}"
  },
  {
    name = "doc/tensors-doc.tex",
    
    version_pattern = module.."Version{v%d+%.%d+%.%d+%}",
    date_pattern = module.."Date{%d%d%d%d%-%d%d%-%d%d}",
    
    version_output =  module.."Version{v".. pkgversion .."}",
    date_output = module.."Date{"..pkgdate.."}"
  },
  {
    name = "ctan/ctan.ann",
    version_pattern = "Version: %d+%.%d+%.%d+",
    date_pattern = "Date: %d%d%d%d%-%d%d%-%d%d",
    version_output = "Version: "..pkgversion,
    date_output = "Date: "..pkgdate,
  },
  {
    name = "CHANGELOG.md",
    version_pattern = "Version: %d+%.%d+%.%d+",
    date_pattern = "Date: %d%d%d%d%-%d%d%-%d%d",
    version_output = "Version: "..pkgversion,
    date_output = "Date: "..pkgdate,
    onlyone_check = true,
  }
}


-- Function to replace version and date in the target files
local function update_version_date(writeFilesBool)
  -- Assign default values if arguments are not provided (i.e., nil)
  writeFilesBool = writeFilesBool or true
  
  -- Iterate over each file and apply specific patterns
  for _, file in ipairs(files2tag) do
    -- Read the file content
    local f = io.open(file.name, "r")
    if not f then
      print("Error: Could not open file: " .. file.name)
      goto continue -- Skip to the next file
    end
    local content = f:read("*all")
    f:close()

    -- Track if changes were made
    local changes_made = false

    -- Verify and replace version
    if file.version_pattern then
      if string.match(content, file.version_pattern) then
        if file.version_subpattern then
          -- Replace only the version part
          content = string.gsub(content, file.version_pattern, 
            function(line)
              return string.gsub(line, file.version_subpattern, file.version_output)
            end
          )
        else  
          content = string.gsub(content, file.version_pattern, file.version_output)
        end
        changes_made = true
      else
        print("Error: Version pattern not recognized in file: " .. file.name)
      end
    end

    -- Verify and replace date
    if file.date_pattern then
      if string.match(content, file.date_pattern) then
        if file.date_subpattern then
          -- Replace only the date part, after version is already replaced
          content = string.gsub(content, file.date_pattern, 
            function(line)
              return string.gsub(line, file.date_subpattern, file.date_output)
            end
          )
        else
          content = string.gsub(content, file.date_pattern, file.date_output)
        end
        changes_made = true
      else
        print("Error: Date pattern not recognized in file: " .. file.name)
      end
    end

    -- If changes were made, write back to the file
    if changes_made then
      if file.onlyone_check then
        print("Warn: The version and date in: "..file.name.." is 'onlyone_check', ie. has to be updated MANUALLY !!" )
        -- os.exit(2)
      else
        f = io.open(file.name, "w")
        f:write(content)
        f:close()
        print("Updated version and date in: " .. file.name)
      end
    else
      print("No changes made to: " .. file.name)
    end

    ::continue::
  end
end

-- Create check_marked_tags() function
local function check_marked_tags()
  retCode=0

  -- Iterate over each file and apply specific patterns
  for _, file in ipairs(files2tag) do
    -- Read the file content
    local f = io.open(file.name, "r")
    if not f then
      print("Error: Could not open file: " .. file.name)
      goto continue -- Skip to the next file
    end
    local content = f:read("*all")
    f:close()

    -- Verify and replace version
    if file.version_pattern then
      retCodeTemp=0
      if string.match(content, file.version_pattern) then
        for line in string.gmatch(content, file.version_pattern) do
          if file.version_subpattern then
              lineOut = string.gsub(line, file.version_subpattern, file.version_output)
          else
            lineOut = string.gsub(line, file.version_pattern, file.version_output)
          end
          if line==lineOut then
            print("[OK] Version tag in file: " .. file.name.." are matching.")
            -- retCodeTemp=0
          else
            print("Warning: Unmatching version tag in file: " .. file.name .." at line: "..line.."\n")
            retCodeTemp=2
          end
          if file.onlyone_check then break end
        end
      else
        print("Warning: Version pattern not recognized in file: " .. file.name)
        retCodeTemp=2
      end
      retCode=math.max(retCode,retCodeTemp)
    end

    -- Verify and replace date
    if file.date_pattern then
      retCodeTemp=0
      if string.match(content, file.date_pattern) then
        for line in string.gmatch(content, file.date_pattern) do
          if file.date_subpattern then
              lineOut = string.gsub(line, file.date_subpattern, file.date_output)
          else
            lineOut = string.gsub(line, file.date_pattern, file.date_output)
          end
          if line==lineOut then
            print("[OK] Date tag in file: " .. file.name.." are matching.")
            -- retCodeTemp=0
          else
            print("Warning: Unmatching date tag in file: " .. file.name .." at line: "..line.."\n")
            retCodeTemp=2
          end
          if file.onlyone_check then break end
        end
      else
        print("Warning: Date pattern not recognized in file: " .. file.name)
        retCodeTemp=2
      end
      retCode=math.max(retCode,retCodeTemp)
    end

    ::continue::
  end

  os.exit(retCode)
end

-- Add the verification after the tag.
function tag_hook(tagname)
  print("INFO: previous tagging can be dry, so let's check again!")
  check_marked_tags()
end

-- Add "checktag" target to l3build CLI to verfy if tags are the same.
if options["target"] == "checktag" then
  retCode= check_marked_tags()
  os.exit(retCode)
end

-- Hook the function to a custom target
if options["target"] == "updatetag" then
  update_version_date()
  os.exit(0)
end

-- Add target to print the version of the package
if options["target"] == "pkgversion" then
  print("Package: "..module)
  print("Version: "..pkgversion)
  print("Date: "..pkgdate)
  os.exit(0)
end


-- EoF